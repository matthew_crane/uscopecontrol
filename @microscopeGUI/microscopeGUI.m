classdef microscopeGUI<handle
    properties
        figure = [];
        expPanel
        posPanel
        
        expNameText
        rootFolderLabel
        rootFolderText
        selectRootFolderButton
        runExpButton
        stopExpButton
        
        posList
        replaceXYButton
        addendXYButton
        tileXYButton
        goToXYButton
        
        microscope
    end % properties
    %% Displays timelapse for a single trap
    %This can either dispaly the primary channel (DIC) or a secondary channel
    %that has been loaded. It uses the trap positions identified in the DIC
    %image to display either the primary or secondary information.
    methods
        function uscopeGUI=microscopeGUI(microscope,title)
            
            uscopeGUI.microscope=microscope;
            
            if nargin<2
                title='Microscope GUI';
            end
            
            scrsz = get(0,'ScreenSize');
            uscopeGUI.figure=figure('MenuBar','none','Name',title,'Position',[scrsz(3)/3 scrsz(4)/3 scrsz(3)/2 scrsz(4)/2]);
            uscopeGUI.expPanel = uipanel('Parent',uscopeGUI.figure,...
                'Position',[.015 .78 .95 .19 ]);
            uscopeGUI.posPanel = uipanel('Parent',uscopeGUI.figure,...
                'Position',[.015 .02 .7 .75]);
            
%             uscopeGUI.rootFolderLabel = uicontrol(uscopeGUI.expPanel,'Style','text','String','Root Foler of Experiment',...
%                 'Units','normalized','Position',[.025 .625 .4 .3]);
            if isempty(microscope.acqFolder)
                stringFolder='No Folder Selected Yet';
            else
                stringFolder=microscope.acqFolder;
            end
            uscopeGUI.rootFolderText = uicontrol(uscopeGUI.expPanel,'Style','edit','String',stringFolder,...
                'Units','normalized','Position',[.005 .55 .6 .45],'Callback',@(src,event)setRootFolder(uscopeGUI));
            uscopeGUI.expNameText = uicontrol(uscopeGUI.expPanel,'Style','edit','String','DefaultExpName',...
                'Units','normalized','Position',[.005 .055 .4 .45],'Callback',@(src,event)setExpName(uscopeGUI));

            uscopeGUI.selectRootFolderButton = uicontrol(uscopeGUI.expPanel,'Style','pushbutton','String','Select Exp Root',...
                'Units','normalized','Position',[.41 .025 .2 .45],'Callback',@(src,event)selectRootFolder(uscopeGUI));
            uscopeGUI.runExpButton = uicontrol(uscopeGUI.expPanel,'Style','pushbutton','String','Run Experiment',...
                'Units','normalized','Position',[.615 .025 .2 .95],'Callback',@(src,event)runExp(uscopeGUI));
            uscopeGUI.stopExpButton = uicontrol(uscopeGUI.expPanel,'Style','pushbutton','String','Stop Experiment',...
                'Units','normalized','Position',[.835 .025 .15 .95],'Callback',@(src,event)stopExp(uscopeGUI));

            
            uscopeGUI.posList = uicontrol(uscopeGUI.posPanel,'Style','listbox','String',{'None Loaded'},...
                'Units','normalized','Position',[.025 .025 .65 .95],'Max',30,'Min',1);
            uscopeGUI.updatePosList;
            
            uscopeGUI.addendXYButton = uicontrol(uscopeGUI.posPanel,'Style','pushbutton','String','Mark XYZ',...
                'Units','normalized','Position',[.7 .8 .28 .2],'Callback',@(src,event)addendXY(uscopeGUI));
            
            uscopeGUI.replaceXYButton = uicontrol(uscopeGUI.posPanel,'Style','pushbutton','String','Replace XYZ',...
                'Units','normalized','Position',[.7 .6 .28 .2],'Callback',@(src,event)replaceXY(uscopeGUI));

            uscopeGUI.goToXYButton = uicontrol(uscopeGUI.posPanel,'Style','pushbutton','String','Go to Pos',...
                'Units','normalized','Position',[.7 .3 .28 .2],'Callback',@(src,event)goToXY(uscopeGUI));

            uscopeGUI.tileXYButton = uicontrol(uscopeGUI.posPanel,'Style','pushbutton','String','Tile XYZ',...
                'Units','normalized','Position',[.7 .05 .28 .2],'Callback',@(src,event)tileXY(uscopeGUI));

            set(uscopeGUI.stopExpButton, 'Enable', 'Off');  

        end
        
        function runExp(uscopeGUI)
            set(uscopeGUI.runExpButton, 'Enable', 'Off');
            set(uscopeGUI.stopExpButton, 'Enable', 'On');
            uscopeGUI.microscope.runAcq;
        end
        
        function stopExp(uscopeGUI)
            stop(uscopeGUI.microscope.loopTimer);
            fclose(uscopeGUI.microscope.logFile);
            set(uscopeGUI.runExpButton, 'Enable', 'On');
            set(uscopeGUI.stopExpButton, 'Enable', 'Off');

        end
        
        function updatePosList(uscopeGUI)
            posList={};
            for i=1:size(uscopeGUI.microscope.posXYZ,1)
                posList{i}=['Pos ' num2str(i) '  -  X:' num2str(uscopeGUI.microscope.posXYZ(i,1)) ...
                    ' Y:' num2str(uscopeGUI.microscope.posXYZ(i,2)) ...
                    ' Z: ' num2str(uscopeGUI.microscope.posXYZ(i,3))];
            end
            set(uscopeGUI.posList,'String',posList);
        end
        
        function selectRootFolder(uscopeGUI)
            uscopeGUI.microscope.setAcqFolder;
            set(uscopeGUI.rootFolderText,'string',uscopeGUI.microscope.acqFolder);
        end
        
        function setRootFolder(uscopeGUI)
            uscopeGUI.microscope.acqFolder=get(uscopeGUI.rootFolderText,'String');
        end
        
        function setExpName(uscopeGUI)
            uscopeGUI.microscope.originalExpName=get(uscopeGUI.expNameText,'String');
            uscopeGUI.microscope.expName=uscopeGUI.microscope.originalExpName;
        end

        
        function goToXY(uscopeGUI)
            currPos=get(uscopeGUI.posList,'Value');
            uscopeGUI.microscope.currPos=currPos;
            tL=uscopeGUI.microscope.shouldWriteLog;
            uscopeGUI.microscope.shouldWriteLog=false;
            xloc=uscopeGUI.microscope.posXYZ(currPos,1);
            yloc=uscopeGUI.microscope.posXYZ(currPos,2);
            zloc=uscopeGUI.microscope.posXYZ(currPos,3);

            uscopeGUI.microscope.visitXY(xloc,yloc);
            uscopeGUI.microscope.visitZ(zloc);
            uscopeGUI.microscope.shouldWriteLog=tL;
        end
        
        function replaceXY(uscopeGUI)
            currPos=get(uscopeGUI.posList,'Value');
            uscopeGUI.microscope.currPos=currPos;
            uscopeGUI.microscope.markXYZ;
            uscopeGUI.updatePosList;
        end
        
        function addendXY(uscopeGUI)
            if isempty(uscopeGUI.microscope.posXYZ)
                cs=0;
            else
                cs=size(uscopeGUI.microscope.posXYZ,1);
            end
            uscopeGUI.microscope.currPos=cs+1;
            uscopeGUI.microscope.markXYZ;
            uscopeGUI.updatePosList;
        end
        
        function tileXY(uscopeGUI)
            uscopeGUI.microscope.tileXY;
            uscopeGUI.updatePosList;
        end
        
    end
end
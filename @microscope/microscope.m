classdef microscope<handle
    properties
        
        currFilter
        currPos
        currFocusOffset
        currXY
        currFrame %absolute frame from start of the experiment
        loopFrame %current Frame in the loop
        currLoop %current loop
        currCh %current channel number
        
        logFile
        logString
        shouldWriteLog %bool true/false for whether the log should be written
        acqFolder %folder where images should be saved
        expName %name of the experiment
        originalExpName
        loopParams
        loopTimer
        
        name
        afMethod
        afOn %bool on/off
        afParams
        afStack
        afRelLoc
        xyDrive
        zDrive
        imWidth
        imHeight
        
        posXYZ %matrix where each row is position, and columns are x,y,z
        posOffset %
    end
    
    methods
        
        function scope=microscope(name)
            switch name
                case 'Nikon'
                    scope.afMethod='PFS';
                    scope.xyDrive='TIXYDrive';
                    scope.zDrive='TIZDrive';
                    scope.imWidth=2048;
                    scope.imHeight=2048;
                case 'Zeiss'
                    scope.afMethod='software';
                    scope.xyDrive='XYStage';
                    scope.zDrive='Focus';
                    scope.imWidth=1040;
                    scope.imHeight=1388;
            end
            scope.afParams.umRange=6;
            scope.afParams.umSteps=.5;
            scope.afParams.umRangeRefocus=12;
            scope.afParams.umStepsRefocus=1;

            scope.name=name;
            scope.currPos=1;
            scope.currFocusOffset=[];
            scope.currFrame=1;
            scope.currLoop=1;
            scope.currCh=1;
            scope.loopFrame=1;
            
            
            scope.loopParams=struct('numFrames',[],'interval',[],'channels',{'BF'},'exposureTime',[],'skip',[],...
                'acqStack',[],'zSliceN',[],'zSliceStep',[],'zOffset',[]);
            %numFrames - frames to acquire in that part of the loop [1]
            %channels - 'GFP','DIC' [1 x N]
            %exposureTime - exp time for each of the channels [1 x N]
            %skip - frames to skip (1 is no skip) [1 x N]
            %acqStack - bool (true is acq stack for that channel, false is
            %don't) [1 x N]
            %zSliceN - sliceNumber for that channel [1 x N]
            %zSliceStep - step size for the slices [1 x N]
            %zOffset - should this chhannel start at the default focus or
            %use a different one
        end
        
        function refocusZPos(scope,posNum)
            currPos=posNum;
            tL=scope.shouldWriteLog;
            scope.shouldWriteLog=false;
            tempRange=scope.afParams.umRange;
            tempSteps=scope.afParams.umSteps
            scope.afParams.umRange=scope.afParams.umRangeRefocus;
            scope.afParams.umSteps=scope.afParams.umStepsRefocus;
            scope.visitPosUpdateAF(posNum);
            scope.shouldWriteLog=tL;
            scope.afParams.umRange=tempRange;
            scope.afParams.umSteps=tempSteps;

        end
        
        function refocusAllZPos(scope)
            for i=1:size(scope.posXYZ,1)
                scope.refocusZPos(i);
            end
        end
        
        function setExpName(scope,expName,acqFolder)
            global mmc
            if nargin<2 || isempty(expName)
                [expName,acqFolder] = uiputfile;
            end
            scope.originalExpName=expName;
            scope.expName=expName;
            scope.acqFolder=acqFolder;
        end
        
        
        function setAcqFolder(scope,acqFolder)
            if nargin<2 || isempty(acqFolder)
                acqFolder = uigetdir(scope.acqFolder);
            end
            if acqFolder
                scope.acqFolder=acqFolder;
            end
        end
        
        function setLogFile(scope,logName)
            global mmc
            if nargin<2 || isempty(logName)
                logName = uiputfile;
            end
            scope.logFile=fopen(logName,'w');
        end
        
        
        
    end
end
